package org.example.java18;

public class Snippet {

    /**
     * A simple program.
     * {@snippet :
     *   public class HelloWorld {
     *     public static void main(String... args) {
     *       System.out.println("Hello World!"); // @highlight substring="println"
     *     }
     *   }
     * }
     */
    public void foo() {
    }
}
