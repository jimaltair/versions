package org.example.java19;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class AdditionalDateTimeFormats {

    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedPattern("yMMM");
        System.out.println("US:      " + formatter.withLocale(Locale.US).format(now));
        System.out.println("Germany: " + formatter.withLocale(Locale.GERMANY).format(now));
        System.out.println("Japan:   " + formatter.withLocale(Locale.JAPAN).format(now));
    }
}
