package org.example.java19;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

public class Task implements Callable<Integer> {

    private final int number;

    public Task(int number) {
        this.number = number;
    }

    @Override
    public Integer call() {
        System.out.printf("Thread %s - Task %d waiting...%n", Thread.currentThread().getName(), number);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.printf("Thread %s - Task %d canceled.%n", Thread.currentThread().getName(), number);

            return -1;
        }

        System.out.printf("Thread %s - Task %d finished.%n", Thread.currentThread().getName(), number);

        return ThreadLocalRandom.current().nextInt(100);
    }

    // classic threads vs. virtual threads
    public static void main(String[] args) {
//         try (ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor()) {
        try (ExecutorService executor = Executors.newFixedThreadPool(100)) {
            List<Task> tasks = new ArrayList<>();
            for (int i = 0; i < 1_000; i++) {
                tasks.add(new Task(i));
            }

            long startTime = System.currentTimeMillis();

            List<Future<Integer>> futures = executor.invokeAll(tasks);

            long sum = 0;
            for (Future<Integer> future : futures) {
                sum += future.get();
            }

            long duration = System.currentTimeMillis() - startTime;

            System.out.printf("sum = %d, duration = %d ms", sum, duration);
        } catch (ExecutionException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    // how to create virtual threads
    public void foo() {
        Thread.startVirtualThread(() -> {
            // code to run in thread
        });

        Thread.ofVirtual().start(() -> {
            // code to run in thread
        });

        // check if code is running in a virtual thread
        boolean isVirtual = Thread.currentThread().isVirtual();
    }

}
