package org.example.java19;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.WeakHashMap;

public class NewMethodsToCreateMapsAndSets {

    /**
     * Return the map with real capacity that actually bigger than int-argument from constructor to prevent resizing
     * due to load factor. For example, for map with number of mappings 16 the real size is 22.
     */
    public static void main(String[] args) {
        HashMap<Object, Object> map1 = HashMap.newHashMap(16);
        LinkedHashMap<Object, Object> map2 = LinkedHashMap.newLinkedHashMap(16);
        WeakHashMap<Object, Object> map3 = WeakHashMap.newWeakHashMap(16);
        HashSet<Object> set1 = HashSet.newHashSet(16);
        LinkedHashSet<Object> set2 = LinkedHashSet.newLinkedHashSet(16);
    }
}
