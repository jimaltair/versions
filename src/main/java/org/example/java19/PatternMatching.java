package org.example.java19;

public class PatternMatching {

    // pattern matching with keyword 'when'
    private String formatterPatternSwitch(Object o) {
        return switch (o) {
            case Integer i when i > 0 -> String.format("int %d", i);
            default -> o.toString();
        };
    }

    record Position(int x, int y) {
    }

    record Path (Position from, Position to) {
    }

    // record pattern
    private void print1(Object object) {
        if (object instanceof Position position) {
            System.out.printf("object is a position, x = %d, y = %d", position.x(), position.y());
        }
    }

    // record pattern with 'instanceof'
    private void print2(Object object) {
        if (object instanceof Position(int x, int y)) {
            System.out.printf("object is a position, x = %d, y = %d", x, y);
        }
    }

    // record pattern with 'switch'
    private void print3(Object object) {
        switch (object) {
            case Position(int x, int y) -> System.out.printf("object is a position, x = %d, y = %d", x, y);
            default -> {}
        }
    }

    // nested record pattern with 'instanceof'
    private void print4(Object object) {
        if (object instanceof Path(Position(int x1, int y1), Position(int x2, int y2))) {
            System.out.printf("object is a path, x1 = %d, y1 = %d, x2 = %d, y2 = %d", x1, y1, x2, y2);
        }
    }

    // nested record pattern with 'switch'
    private void print5(Object object) {
        switch (object) {
            case Path(Position(int x1, int y1), Position(int x2, int y2)) ->
                    System.out.printf("object is a path, x1 = %d, y1 = %d, x2 = %d, y2 = %d", x1, y1, x2, y2);
            default -> {}
        }
    }
}
