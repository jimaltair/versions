package org.example.java21;

public class UnnamedPatternsAndVariables {

    private void unnamedVariable(Integer[] intArray) {
        int limit = 42; // some const
        int acc = 0;
        for (Integer _ : intArray) {
            if (acc < limit)
                acc++;
        }

        String s = "42";
        try {
            int i = Integer.parseInt(s);
        } catch (NumberFormatException _) {
            System.out.println("Bad number: " + s);
        }
    }

    record Point(int x, int y) {}
    enum Color {RED, GREEN, BLUE}
    record ColoredPoint(Point p, Color c) {}
    record Box<T>(T t) {}

    // unnamed variable with 'instanceof'
    private void unnamedPatterns1(Object obj) {
        if (obj instanceof ColoredPoint(Point(int x, int y), _)) {
            System.out.println(x + y);
        }
    }

    // unnamed variable with 'switch'
    private <T> void unnamedPatterns2(Box<T> box) {
        switch (box) {
            case Box(Integer _) -> {}
            case Box(Point _), Box(Color _) -> {}
            default -> {}
        }
    }
}
