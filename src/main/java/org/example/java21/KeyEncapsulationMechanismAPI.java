package org.example.java21;

import javax.crypto.DecapsulateException;
import javax.crypto.KEM;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class KeyEncapsulationMechanismAPI {

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, DecapsulateException {
        // Receiver side
        var kpg = KeyPairGenerator.getInstance("X25519");
        var kp = kpg.generateKeyPair();

        // Sender side
        var kem1 = KEM.getInstance("DHKEM");
        var sender = kem1.newEncapsulator(kp.getPublic());
        var encapsulated = sender.encapsulate();
        var k1 = encapsulated.key();

        // Receiver side
        var kem2 = KEM.getInstance("DHKEM");
        var receiver = kem2.newDecapsulator(kp.getPrivate());
        var k2 = receiver.decapsulate(encapsulated.encapsulation());

        assert Arrays.equals(k1.getEncoded(), k2.getEncoded());
    }
}
