package org.example.java21;

/**
 * Finalized version
 */
public class PatternMatching {

    private String test1(Object obj) {
        return switch (obj) {
            // if this branch is absent, we get NPE, even with default branch
            case null -> "null";
            case Integer i when i > 0 -> String.format("positive int %d", i);
            case Integer i -> String.format("int %d", i);
            case Long l -> String.format("long %d", l);
            case Double d -> String.format("double %f", d);
            case String s -> String.format("String %s", s);
            // branches with null and default can be merged
//            case null, default -> "Null or other";
            default -> obj.toString();
        };
    }

    record Point(int x, int y) {}
    enum Color { RED, GREEN, BLUE }
    record ColoredPoint(Point p, Color c) {}
    record Rectangle(ColoredPoint upperLeft, ColoredPoint lowerRight) {}

    // record deconstruction with 'instanceof'
    private void printSum1(Object obj) {
        if (obj instanceof Point(int x, int y)) {
            System.out.println(x + y);
        }
    }

    // record deconstruction with 'switch'
    private void printSum2(Object obj) {
        switch (obj) {
            case Point(int x, int y) -> System.out.println(x + y);
            default -> System.out.println("Not a point");
        }
    }

    // deconstruction of nested records
    private void printColorOfUpperLeftPoint(Rectangle r) {
        if (r instanceof Rectangle(ColoredPoint(var p, var c), var lr)) {
            System.out.println(c);
        }
    }

    record Box<T>(T t) {}

    //record patterns with type inference
    private void test(Box<Box<String>> box) {
        if (box instanceof Box(Box(var s))) { // Infers Box<Box<String>>(Box<String>(String s))
            System.out.println("String " + s);
        }
    }

    //unfortunately, not works in java21
/*    private void usePoint(Point p) {
        Point(var x, var y) = p; // Не сработает
        // Use x and y
    }*/
}
