package org.example.java21;

import java.util.ArrayDeque;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SequencedCollections {

    private void test() {
        // implements SequencedSet interface
        var set = new LinkedHashSet<>(Set.of(1, 2, 3));
        set.getFirst();
        set.getLast();
        set.addFirst(4);
        set.addLast(4);
        set.reversed();

        // implements SequencedMap interface
        var map = new LinkedHashMap<>(Map.of(1, "one", 2, "two", 3, "three"));
        map.firstEntry();
        map.lastEntry();
        map.putFirst(4, "four");
        map.putLast(4, "four");
        map.reversed();

        // implements Deque interface, Deque implements SequencedCollection
        var deque = new ArrayDeque<>(List.of(1, 2, 3));
        deque.getFirst();
        deque.getLast();
        deque.addFirst(4);
        deque.addLast(4);
        deque.reversed();
    }
}
