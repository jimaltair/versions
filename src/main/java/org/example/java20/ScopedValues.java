package org.example.java20;

/**
 * Scoped values - values that may be safely and efficiently shared to methods without using method parameters.
 * They are preferred to thread-local variables, especially when using large numbers of virtual threads.
 */
public class ScopedValues {

/*    private static final ScopedValue<FrameworkContext> CONTEXT = ScopedValue.newInstance();

    void serve(Request request, Response response) {
        var context = createContext(request);
        ScopedValue.where(CONTEXT, context)
                .run(() -> Application.handle(request, response));
    }

    public PersistedObject readKey(String key) {
        var context = CONTEXT.get();
        var db = getDBConnection(context);
        db.readKey(key);
    }*/
}
