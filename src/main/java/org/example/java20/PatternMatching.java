package org.example.java20;

public class PatternMatching {

    record Box<T>(T t) {
    }

    private void test1(Box<String> box) {
        if (box instanceof Box(var s)) {
            System.out.println("String " + s);
        }
    }

    record Point(int x, int y) {
    }

    // deleted in java21
/*    private void test2(Point[] pointArray) {
        for (Point(var x, var y) : pointArray) {
            System.out.printf("Point(x = %d, y = %d)\n", x, y);
        }
    }*/

    record Pair<S, T>(S first, T second) {}

    private void recordInference(Pair<String, Integer> pair) {
        switch (pair) {
            case Pair(var fst, var snd) -> {} // converts to Pair<String, Integer>
            default -> {}
        }
    }
}
